/**
 * Created by Administrator on 2017/3/12.
 */
//用来改变窗口的html字体大小，从而利用rem影响窗口的整体布局
//需结合jq使用
$(function(){
    $(window).on('resize',function(){
        var $width = $(window).width();
        //console.log($width);
        var $fontSize = 100/640*$width;
        $('html').css('fontSize',$fontSize);//给页面的html重新赋值；
    }).trigger('resize');
});